# Dokcer Demó -14. feladat

> ***docker build with simple php app***

## Hozzunk létre egy mappát a demo-php docker build-hez 

```
mkdir /oktatas/docker/demo-php
cd /oktatas/docker/demo-php
```

## Töltsük le a git-böl a demo php alkalmazást 

```
git clone https://gitlab.com/cspaldi/demo-php-app.git .
```
## Hozzunk létre a demo-php mappába a Dockerfile-t a docker build-hez

***FYI!*** Az ***APPVER*** változóhoz adj meg tetszőleges értéket!

```
FROM php:apache

ENV APPVER=1.21

WORKDIR /var/www/html
COPY ./html/   .
EXPOSE 80
```

## Készítsük el a saját image-et és tag-nek adjuk meg az ***APPVER**-ben megadott értéket

```
docker build . -t registry.gitlab.com/cspaldi/docker-demo/myapp:1.21
```

## Készítsünk egy docker-compose.yml fájlt a saját image-ből történő konténer indításához

```
version: '3.1'
services:
  myapp-container:
    container_name: myapp
    image: registry.gitlab.com/cspaldi/docker-demo/myapp:1.21
    ports:
      - 8080:80
```
## Indítsuk el a myapp konténert és ellenőrizzük egy web browserben az eredményt

```
docker-compose up -d
```
***FYI!*** Ellenőrizzük az APPVER értékét!

## Módosítsuk a docker-compose.yml fájlt az ***enviroment***-el és adjunk új ***APPVER*** értéket

```
version: '3.1'
services:
  myapp-container:
    container_name: myapp
    image: registry.gitlab.com/cspaldi/docker-demo/myapp:1.21
    ports:
      - 8080:80
    environment:
        APPVER: 1.xx  
```
## Indítsuk újra a myapp konténert és ellenőrizzük egy web browserben az eredményt

```
docker-compose up -d
```
***FYI!*** Ellenőrizzük az APPVER értékét!

## Állítsuk le a myapp konténert

```
docker-compose down
```
