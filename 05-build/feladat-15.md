# Dokcer Demó -15. feladat

> ***docker build - Wordpress***
## Hozzunk létre egy mappát a demo-wordpress néven

```
mkdir /oktatas/docker/demo-wordpress
cd /oktatas/docker/demo-wordpress
```

## Töltsük le a ***wordpress 6.0***-es verzióját és tömörítsük ki

```
wget https://wordpress.org/wordpress-6.0.tar.gz
tar -xzf wordpress-6.0.tar.gz
```

## Nevezzük át a ***wp-config-sample.php***-t ***wp-config.php***-ra

```
cd wordpress
mv wp-config-sample.php wp-config.php
```

## Módosítsuk a ***wordpress*** mappában a ***wp-config.php***-t

***FYI!*** A módosítandó paraméterek:
- DB_NAME: wordpress
- DB_USER: wpuser
- DB_PASSWORD: wp_passwd
- DB_HOST: wpdb

## Hozzunk létre egy ***.htaccess*** nevű fájlt

```
cat >.htaccess<<EOF 
# BEGIN WordPress
<IfModule mod_rewrite.c>
RewriteEngine On
RewriteRule .* - [E=HTTP_AUTHORIZATION:%{HTTP:Authorization}]
RewriteBase /
RewriteRule ^index\.php$ - [L]
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule . /index.php [L]
</IfModule>
# END WordPress
EOF
```
***FYI!*** Nem létkérdés, de állitsul át a *.htaccess* tulajdonosát is *nobody*-ra

## Hozzunk létre a ***SAJÁT*** GitLab-ban egy új project-et ***wordpress-demo*** néven

***FYI!*** A git url-nél mindenki a ***SAJÁT*** git usernevét adja meg!

```
git init
git remote add origin https://gitlab.com/cspaldi/wordpress-demo.git
git add .
git commit -m 'Added WordPress 6.0 files'
git push --set-upstream origin master
```

**Ideiglenesen állítsuk át a GitLab gui-n a *wordpress-demo* project-et publikusra!**


## Hozzunk létre egy ***build*** nevű mappát a docker build-hez 

***FYI!*** Azért NE a wordpress mappába tegyük, mert azt git-el kezeljük.

```
mkdir /oktatas/docker/demo-wordpress/build
cd /oktatas/docker/demo-wordpress/build
```
## Hozzunk létre a Dockerfile-t a docker build-hez

```
FROM registry.gitlab.com/cspaldi/docker-demo/apache-php:1.0
 
RUN apt update &&  \
    apt install -y git && \
    apt clean && apt autoclean

WORKDIR /var/www/html

RUN git clone https://gitlab.com/cspaldi/wordpress-demo.git .

EXPOSE 80
```

## Készítsük el a saját image-et ***mywp:1.0*** néven

```
docker build . -t mywp:1.0
```

## Készítsünk egy docker-compose.yml fájlt a saját image-ből történő konténer indításához

```
version: '3.1'
services:
  wordpress-container:
    container_name: mywp
    image: mywp:1.0
    ports:
      - 80:80
    depends_on:
      - wordpress_db

  wordpress_db:
    container_name: wpdb
    image: mariadb:10
    environment:
      MARIADB_ROOT_PASSWORD: Secret
      MARIADB_DATABASE: wordpress
      MARIADB_USER: wpuser
      MARIADB_PASSWORD: wp_passwd
    volumes:
      - mysqldb:/var/lib/mysql

volumes:
  mysqldb:
```
## Indítsuk el a konténereinket és ellenőrizzük egy web browserben az eredményt

```
docker-compose up -d
```
