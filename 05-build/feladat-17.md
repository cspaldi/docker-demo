# Dokcer Demó - 17. feladat

> ***Takarítás***

## Állítsunk le MINDEN futó konténert

```
docker kill $(docker ps -q)
```

## Töröljünk ki MINDEN leállított konténert

```
docker rm $(docker ps -qa)
```

## Töröljünk ki MINDEN image fájlt a lokális registry-ből

```
docker rmi $(docker images -q)
docker images
```
**FYI!** Az azonos ID-jű, de eltérő tag-el rendelkező image-eket nem törli, mert a "docker images -q" parancs csak az image nevét adja vissza és több azonos nevű is van!

Az alábbi paranccsal lehet egy lépésben az ÖSSZES, akár azonos nevű image-et törölni

````
docker image prune -af
````
A **-a** kapcsoló jelentése: **all**
A **-f** kapcsoló jelentése: **force**

## Töröljünk ki MINDEN volume-ot és network-öt 

```
docker volume prune -f
docker network prune -f
```








