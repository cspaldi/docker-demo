# Dokcer Demó -16. feladat

> ***Update Wordpress***
## lépjünk vissza a demo-wordpress mappába

```
cd /oktatas/docker/demo-wordpress
```

## Töltsük le a ***wordpress 6.0.1***-es verzióját és tömörítsük ki

```
wget https://wordpress.org/wordpress-6.0.1.tar.gz
tar -xzf wordpress-6.0.1.tar.gz
```


## Küldük fe a módosításokat a ***wordpress-demo*** projektükbe a GitLab-abnéven

***FYI!*** A git url-nél mindenki a ***SAJÁT*** git usernevét adja meg!

```
cd wordpress
git add .
git commit -m 'Upgraded to WordPress 6.0.1'
git push
```

***FYI!*** Most felül írtuk a git-ben a Master-t! Egy rendes környezetben készítünk egy Test branch-et, oda rakjuk fel, teszteljük, és ha minden O.K, csak akkor merge-öljük össza a Masterrel!


## Lépjünk be a ***build*** mappába és készítsük el a saját ***mywp:2.0*** image-t

```
cd ../build
docker build . --no-cache -t mywp:2.0 -t mywp:latest
```

***FYI!*** Azért kell a --no-cache, mert a build cache-el, és van egy olyan letárolt layer, ahol a git clone-t végrehajtotta. Arról fogalma sincs, hogy fent a git-ben változtak a fájlok, ezért a letárolt layert használná!

## Módosítsuk a docker-compose.yml fájlban az image nevét ***2.0***-ra

```
version: '3.1'
services:
  wordpress-container:
    container_name: mywp
    image: mywp:2.0
    ports:
      - 80:80
    links:
      - wordpress_db:wpdb
    depends_on:
      - wordpress_db

  wordpress_db:
    container_name: wpdb
    image: mariadb:10
    environment:
      MARIADB_ROOT_PASSWORD: Secret
      MARIADB_DATABASE: wordpress
      MARIADB_USER: wpuser
      MARIADB_PASSWORD: wp_passwd
    volumes:
      - mysqldb:/var/lib/mysql

volumes:
  mysqldb:
```
## Indítsuk el a konténereinket és ellenőrizzük egy web browserben az eredményt

```
docker-compose up -d
```
