# Dokcer Demó - 6. feladat

> ***Tag-ek és registry-k***

## Kérdezzük le milyen tag-ek vannak a mysql image-hez hozzárendelve a Docker Hub-on

Nyissuk meg egy web böngészöben a következő link-et: 
[Nginx - Official Image | Docker Hub](https://hub.docker.com/_/nginx) 

## Töltsük le a 1.23-ás tag-el ellátott mysql image-t és ellenőrizzük a lokális registry-ben

```
docker pull nginx:1.23
docker images
```
Vegyük észre, hogy mindkét ngix image ID-je megegyezik. Ez ugyan az az image, csak több tag-el van ellátva!


## Adjunk hozzá egy új, "1.x" tag-et a letöltött image-hez és ellenőrizzük  

```
docker tag nginx nginx:1.x
docker images
```

## Töröljük ki az nginx:1.x docker image-et a lokális registry-ből
```
docker rmi nginx:1.x
docker images
```
Csak a tag törlődött, az image megmaradt, mert ugyan arra az image-re több tag is mutat.

## Töltsük fel a saját központ registry-nkbe az nginx:1.23 docker image-et a lokális registry-ből ***my-nginx*** néven

A feltöltéshez először új tag-el kell ellátni az image-t, ahol az image neve elé meg kell adni a saját központi registry url-jét. A tag paranccsal nem csak a verziót, de az image nevet is megváltoztathatjuk! lsd.

***FYI!*** Ne felejtsd átírni **cspaldi** helyett a **SAJÁT** felhasználónevedet a registry URL-ben!

```
docker tag nginx:1.23 registry.gitlab.com/cspaldi/docker-demo/my-nginx:1.23
docker images
```
Ezek után a **push** paranccsal fel tudjuk tölteni a saját központi registry-be, mivel az image neve tartalmazza a registry url-t is.

```
docker push registry.gitlab.com/cspaldi/docker-demo/my-nginx:1.23
```
Amennyiben ez egy publikus registry (nincs authentikáció) az image feltöltödik, ellenkező esetben **denied: access forbidden** üzenetet kapunk.

## Jelentkeztessük be a docker kliensünket a központi private registry-be, majd töltsük fel az image-et

```
docker login registry.gitlab.com
```
***FYI!*** A jelszó hash a felhasználó home könytára alá a .docker/config.json fájlba kerül letárolásra!

````
cat ~/.docker/config.json
````
Tőltsük fe az image-et a központi private registry-be

```
docker push registry.gitlab.com/cspaldi/docker-demo/my-nginx:1.23
```

Ellenőrizzük a gitlab web-es felületén a sikeres feltöltést.

## Jelentkeztessük ki a központi prtivate registry-ből

```
docker logout registry.gitlab.com
```
Ellenőrizzük, hogy a docker eltávolította a jelszó hash-t a config.json fájlból

````
cat ~/.docker/config.json
````

