# Dokcer Demó - 5. feladat

> ***Image-ek kezelése***

## Kérdezzük le milyen docker image-ek vannak a lokális registry-ben

```
docker images
```
## Keressünk image-eket a Docker Hub-on

```
docker search my
```
Minden olyan szóra rákeres ami **my**-al kezdődik, a keresés **case insensitive** és nem csak a **nevek**-ben, de a **leírásokban** is keres.


## Töltsünk le egy busybox docker image-et a lokális registry-be, de ne hozzunk létre belőle konténert 

```
docker pull busybox
```
Ellenőrizzük a letöltést

```
docker images
```

## Töröljük ki a busybox docker image-et a lokális registry-ből
```
docker rmi busybox
```
***Ha egynél több azonos nevű, de eltérő TAG-el rendelkező image van, a törléshez meg kell adni a TAG-et is (pl: busybox:1.2)***


