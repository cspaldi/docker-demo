# Dokcer Demó - 7. feladat

> ***Image-ek lementése, visszatöltése***

## Mentsük ki a my-nginx:1.23 docker image-et a lokális registry-ből

***FYI!*** Ne felejtsd átírni **cspaldi** helyett a **SAJÁT** felhasználónevedet a registry URL-ben!

```
docker save registry.gitlab.com/cspaldi/docker-demo/my-nginx:1.23 -o /tmp/my-nginx.tar
```
***FYI!*** A save, CSAK a lokális registry-ből képes menteni!

## Ellenőrizzük a fálj meglétét

```
ls -la /tmp/my-nginx.tar
```

***FYI!*** A save paranccsal egyszerre több image is kimenthető egyetlen tar fájlba. <br>
***pl.*** *docker save image1 image2 image3 -o allfiles.tgz*

## Nézzük meg, mit tartalmaz a letöltött image fájl

```
tar -tvf /tmp/my-nginx.tar
```

## Töröljük le a registry.gitlab.com/cspaldi/docker-demo/my-nginx:1.23 image-et a ***lokális*** registry-ből

```
docker rmi registry.gitlab.com/cspaldi/docker-demo/my-nginx:1.23
```
## Ellenőrizzük, hogy valóban nincs benne

```
docker images
```

vagy

```
docker images registry.gitlab.com/cspaldi/docker-demo/my-nginx
```

## Töltsük vissza a lokális registry-be a lementett image fájlt

```
docker load -i /tmp/my-nginx.tar
```

## Ellenőrizzük a lokálist registry-ben a visszatöltést

```
docker images
```

**FYI!** Vegyük észre, hogy a lementett és visszatöltött tar fájl tartalmazza metaadatként a registry nevet és a tag-et is!

