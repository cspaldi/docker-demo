# Anyagok, feladatok a docker és docker-compose oktatáshoz


## Az anyagokat az alábbi mappák tartalmazzák

- [ ] [**01-alap** - Feladatok alap docker parancsokhoz](https://gitlab.com/cspaldi/docker-demo/-/tree/main/01-alap)
- [ ] [**02-images** - Feladatok image-ek kezeléséhez](https://gitlab.com/cspaldi/docker-demo/-/tree/main/02-images)
- [ ] [**03-extra** - Feladatok haladó docker parancsokhoz (pl. környezeti változók, volume-ok, stb.)](https://gitlab.com/cspaldi/docker-demo/-/tree/main/03-extra)
- [ ] [**04-compose** - Feladatok docker-compose használatához](https://gitlab.com/cspaldi/docker-demo/-/tree/main/04-compose)
- [ ] [**05-build** - Feladatok saját docker image készítéséhez](https://gitlab.com/cspaldi/docker-demo/-/tree/main/05-build)
- [ ] [**06-configure** - Feladatok hálózat kezeléshez és a docker daemon konfigurálásához](https://gitlab.com/cspaldi/docker-demo/-/tree/main/06-configure)
- [ ] [**07-secops** - Feladatok üzemeltetéshez és biztonsági beállításokhoz](https://gitlab.com/cspaldi/docker-demo/-/tree/main/07-secops)
