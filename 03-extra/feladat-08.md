# Dokcer Demó - 8. feladat

> ***Környezeti változók***

## Indítsunk el egy nginx konténert és hozzunk létre egy MYNAME környezeti változót a konténerbe

```
docker run -d -e MYVAR1=Valami --name web nginx:1.23
```

## Ellenőrizzük, hogy valóban létrejött a változó.

```
docker exec -it web env
```

## Állítsuk le és töröljük el a konténert.

```
docker rm -f web
```

## Hozzunk létre egy környezeti változókat tartalmazó fájlt envfile.txt néven a /tmp alá a következő tartalommal.

```
MYVAR1=Valami
MYVAR2=Akarmi
MYVAR3=Barmi
```
## Indítsunk el egy nginx konténert és használjuk a létrehozott fájlt a környezeti változók konténerbe történő használatához.

```
docker run -d --env-file /tmp/envfile.txt --name web nginx:1.23
```

## Ellenőrizzük, hogy valóban létrejött a változó.

```
docker exec -it web env
```

## Állítsuk le és töröljük el a konténert.

```
docker rm -f web
```
