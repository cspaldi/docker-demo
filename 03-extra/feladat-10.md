# Dokcer Demó - 10. feladat

> ***Volume-ok kezelése***

## Indítsunk el egy postgresql konténert

```
docker run -d --name postgres -e POSTGRES_PASSWORD=Titok postgres 
```

## Lépjünk be a postgresql-be, és hozzunk létre egy *"testdb"* adatbázist

```
docker exec -it postgres sh

# su - postgres
$ psql
postgres=# CREATE DATABASE testdb;
postgres=# \c testdb;
testdb=# CREATE TABLE countries (name varchar(50), shortname varchar(2));
testdb=# INSERT INTO countries(name,shortname) VALUES ('Hungary','HU');
testdb=# SELECT * FROM countries;
testdb=# \q
$ exit
# exit
```
## Állítsuk le és töröljük el a konténerünket

```
docker rm -f postgres
```

## Hozzunk létre újra a postgresql konténert és ellenőrizzük a test adatbázis meglétét

```
docker run -d --name postgres -e POSTGRES_PASSWORD=Titok postgres
docker exec -it postgres sh

# su - postgres
$ psql
postgres=# \c testdb;

connection to server on socket "/var/run/postgresql/.s.PGSQL.5432" failed: FATAL:  database "testdb" does not exist

postgres=#\q
$ exit
# exit
```
## Állítsuk le és töröljük el a konténerünket

```
docker rm -f postgres
```

## Hozzunk létre újra a postgresql konténert és adjunk hozzá egy persistent volume-ot *"dbvol"* néven

```
docker run -d --name postgres -e POSTGRES_PASSWORD=Titok -v dbvol:/var/lib/postgresql/data postgres
```

## Ellenőrizzük le, hogy létre jött-e és ha igen, hol jött létre a ***dbvol*** volume

```
docker volume ls
docker volume inspect dbvol
```


***FYI!*** Jelenleg csak 1 db ***volume driver*** van a dockernek megadva, (***local***), de plugin-okkal több is telepíthető docker alá (pl. nfs). Ha több is lenne, a ***--volume-driver=xxx** kapcsolóval lehet megadni, hogy mit használjon. 

## Lépjünk be a postgresql-be, és hozzunk létre újra a *"testdb"* adatbázist 

```
docker exec -it postgres sh

# su - postgres
$ psql
postgres=# CREATE DATABASE testdb;
postgres=# \c testdb;
testdb=# CREATE TABLE countries (name varchar(50), shortname varchar(2));
testdb=# INSERT INTO countries(name,shortname) VALUES ('Hungary','HU');
testdb=# SELECT * FROM countries;
testdb=# \q
$ exit
# exit
```
## Állítsuk le és töröljük el a konténerünket

```
docker rm -f postgres
```

## Kérdezzük le milyen volum-ok vannak

```
docker volume ls
```

## Hozzunk létre újra a postgresql konténert, adjunk hozzá a *"dbvol"* volume-ot és ellenőrizzük a test adatbázis meglétét

```
docker run -d --name postgres -e POSTGRES_PASSWORD=Titok -v dbvol:/var/lib/postgresql/data postgres
docker exec -it postgres sh

# su - postgres
$ psql
postgres=# \c testdb;
testdb=# SELECT * FROM countries;
testdb=#\q
$ exit
# exit
```

## Állítsuk le és töröljük el a konténerünket

```
docker rm -f postgres
```
## Állítsuk le és töröljük el a *"dbvol"* volume-ot

```
docker volume rm dbvol

// vagy ha több volume is van és mindet tőrőlni akarjuk:

docker volume prune
```







