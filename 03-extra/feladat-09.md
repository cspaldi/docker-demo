# Dokcer Demó - 9. feladat

> ***Bind mount***


## Indítsunk el egy nginx konténert a host gép 8080-as portjára nat-olva

```
docker run -d -p 8080:80 --name web nginx:1.23 
```

## Ellenőrizzük egy web browser-ben a működését, majd állítsuk le és töröljük el a konténert

```
docker rm -f web
```

## Hozzunk létre egy index.html nevű fájlt a /tmp alatt a következő tartalommal

```
<!DOCTYPE html>
<html>
<body>
<h1>Hello docker-demo!</h1>
<p>Ez itt egy volume-bind index.html</p>
</body>
</html>
```

Ellenőrizzük az index.html fájlunk meglétét!

```
ls -ali /tmp/index.html 
```

## Indítsuk egy új nginx-et és csatoljuk fel a létrehozott index.html-t a konténerbe a /usr/share/nginx/html/ alá

```
docker run -d -p 8080:80 --name web -v /tmp/index.html:/usr/share/nginx/html/index.html nginx:1.23 
```

***Ellenőrizzük egy web browser-ben, hogy mit látunk***

## Cseréljük ki ***nano*** szövegszerkesztő segítségével az index.html-ben a ***docker-demo***-t a nevünkre

```
nano /tmp/index.html
```

Ismét ellenőrizzük az index.html fájlunk meglétét, nézzük meg a módosítást dátumát és idejét!

```
ls -ali /tmp/index.html 
```


***Frissítsük a web browser-ben az oldalt, hogy mit látunk***

## Írjuk vissza ***sed*** parancs segítségével az index.html-ben a nevünket ***docker-demo***-ra 

```
sed -i 's/Csaba/docker-demo/' /tmp/index.html
```

***Frissítsük a web browser-ben az oldalt, hogy mit látunk***

Ismét ellenőrizzük az index.html fájlunk meglétét, nézzük meg a módosítást dátumát és idejét!

```
ls -ali /tmp/index.html 
```

**A futó konténer idode-hoz köti a csatolást!!!**

## Állítsuk le és töröljük el az nginx konténert

```
docker rm -f web
```
