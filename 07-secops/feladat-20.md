# Dokcer Demó - 20. feladat

> ***Üzemeltetési kérdések***

## Milyen beállításokkal fut a docker

Adjuk ki a következő parancsot

```
docker info
```
Fontosabb információk.

- deubg mode
- containers
- images
- Storage Driver
- Logging Driver
- Volume

## Logok vizsgálata

Indítsuk el egy nginx konténert

```
docker run -d --name=web -p 80:80 nginx:1.23
```

Nézzük meg a log bejegyzéseket.

```
docker logs web
```

Állítsuk le az nginx konténert.

```
docker stop web
```
Nézzük meg a log bejegyzéseket.

```
docker logs web
```

***FYI!*** A leállt, de el nem törölt konténerek logjai is is lekérdezhetők.

## Hibakeresés ha nem indul a konténer

Indítsunk el egy alpine konténert

```
docker run -d --name=myapp alpine /bin/myapp 2&>/dev/null
```

Nézzük nmeg, hoyg rendben fut-e

```
docker ps
```

Kérdezzük le a konténer log-ját

```
docker logs myapp
```

Nézzük meg az inspect-et a konténert

```
docker inspect myapp
```

***FYI!***  "State": "Error": "failed to create shim task: OCI runtime create failed: runc create failed: unable to start container process: exec: \"/bin/myapp\": stat /bin/myapp: no such file or directory: unknown"
