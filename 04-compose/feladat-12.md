# Dokcer Demó -12. feladat

> ***pull_policy és restart allways***

## Ellenőrizzük, hogy van-e *"nginx:myversion"* nevű konténer image a lokális registry-ben

```
docker images nginx:myversion
```
NINCS ilyen konténer image!

## Hozzunk létre egy *"docker-compose2.yml"* fájlt *"pull_policy: never"* megadásával

```
version: '3.1'
services:
  web-container:
    container_name: web
    image: nginx:myversion
    pull_policy: never
    ports:
      - 8080:80
    environment:
      MYVAR1: valami
      MYVAR2: akarmi
    volumes:
      - /tmp/index.html:/usr/share/nginx/html/index.html
```

## Indítsuk el a konténert

```
docker-compose -f docker-compose2.yml up -d
```

**FYI!** *"Error: No such image: nginx:myversion"* A konténer nem indul el, mert nincs ilyen image a lokális registry-ben!

## Tag-eljük meg az nginix:1.23 image-et *"nginx:myversion"* néven

```
docker tag nginx:1.23 nginx:myversion
docker images nginx:myversion
```

## Indítsuk el a újra a konténert és ellenőrizzük, hogy fut-e

```
docker-compose -f docker-compose2.yml up -d
docker ps
```
**FYI!** A konténer rendben elindul!

## Indítsuk újra a dockert futtató Linux-ot

```
reboot
```
## Lépjünk be az újraindított gépre és ellenőrizzük, hogy fut-e a konténer

```
cd docker
docker ps -a

vagy 

cd docker
docker-compose -f docker-compose2.yml ps
```
**FYI!** A konténer nem fut!

## Módosítsuk a *"docker-compose2.yml"* fájlt a  *"restart: always"* megadásával

```
version: '3.1'
services:
  web-container:
    container_name: web
    image: nginx:myversion
    pull_policy: never
    restart: always
    ports:
      - 8080:80
    environment:
      MYVAR1: valami
      MYVAR2: akarmi
    volumes:
      - /tmp/index.html:/usr/share/nginx/html/index.html
```

## Indítsuk el a újra a konténert és ellenőrizzük, hogy fut-e

```
docker-compose -f docker-compose2.yml up -d
docker ps
```
**FYI!** A konténer rendben elindul!

## Indítsuk újra a dockert futtató Linux-ot

```
reboot
```
## Lépjünk be az újraindított gépre és ellenőrizzük, hogy fut-e a konténer

```
cd docker
docker ps -a

vagy 

cd docker
docker-compose -f docker-compose2.yml ps
```
**FYI!** A konténer reboot után is elindul!

## Állítsuk le a konténert

```
docker-compose -f docker-compose2.yml down
```
