# Dokcer Demó -11. feladat

> ***Simple docker-compose***

## Telepítsük a docker-compose-t a gépünkre (***kuwo-01***)


```
wget https://github.com/docker/compose/releases/download/v2.6.1/docker-compose-linux-x86_64 -O /usr/local/bin/docker-compose
chmod 755 /usr/local/bin/docker-compose
docker-compose --version
```

## Futtassuk docker-compose-al docker helyett az alábbi nginx konténert

***A dockerrel történő indítás így nézett ki:***

docker docker run -d -p 8080:80 --name web -v /tmp/index.html:/usr/share/nginx/html/index.html -e MYVAR1=Valami -e MYVAR2=Akarmi nginx:1.23 

### 1. Hozzuk létre s szükséges docker-compose.yml fájlt ***"docker-compose1.yml"*** néven

```
version: '3.1'
services:
  web-container:
    container_name: web
    image: nginx:1.23
    ports:
      - 8080:80
    environment:
      MYVAR1: valami
      MYVAR2: akarmi
    volumes:
      - /tmp/index.html:/usr/share/nginx/html/index.html
```
**Megj:** Ellenőrizzük a /tmp alatt az index.html meglétét. Ha nincs meg, hozzuk létre újra!

### 2. Indítsuk el docker-compose használatával a konténert
```
docker-compose -f docker-compose1.yml up -d
```

### 3. Ellenőrizzük egy web browser-ben a működését, majd állítsuk le a konténert

```
docker-compose -f docker-compose1.yml down
```
### 3. Ellenőrizzük, hogy van-e leállított nginix konténerünk

```
docker ps -a
```
**FYI!** A docker-compose gondoskodik a leállított konténer törléséről is!
