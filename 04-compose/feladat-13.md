# Dokcer Demó -13. feladat

> ***health-check***

## Módosítsuk a *"docker-compose2.yml"* fájlt a *"healthcheck"* megadásával és mentsük el *"docker-compose3.yml"* néven 

```
version: '3.1'
services:
  web-container:
    container_name: web
    image: nginx:myversion
    pull_policy: never
    restart: always
    ports:
      - 8080:80
    environment:
      MYVAR1: valami
      MYVAR2: akarmi
    volumes:
      - /tmp/index.html:/usr/share/nginx/html/index.html
    healthcheck:
      test: curl -s http://127.0.0.1 | grep -q MyApp
      interval: 10s
      timeout: 3s
      retries: 2
      start_period: 15s
```

## Indítsuk el a konténert és ellenőrizzük a healthcheck állapotát

```
docker-compose -f docker-compose3.yml up -d
watch -n 5 docker ps
```
**FYI!** A konténer ~25 sec után *"unhealthy"* állapotba kerül.

## Állítsuk le a konténert, módosítsuk az index.html header-t MyAppra, majd és idítsuk újra és ellenőrizzük a healthcheck állapotát

```
docker-compose -f docker-compose3.yml stop
sed -i 's/docker-demo/MyApp/' /tmp/index.html
docker-compose -f docker-compose3.yml up -d 
watch -n 5 docker ps
```
**FYI!** A konténer ~15 sec után *"healthy"* állapotba kerül.

## Állítsuk le a konténert

```
docker-compose -f docker-compose3.yml down
```
