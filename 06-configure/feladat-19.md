# Dokcer Demó - 19. feladat

> ***Docker konfigurálása***

## Állítsuk át a docker IP range-et 198.12.0.0/16-ra, /24-es subnetekkel

Hozzunk létre a ***/etc/docker*** alá egy ***daemon.json*** fájlt az alábbi tartalommal:

```
 {
    "default-address-pools": [{
	"base": "198.18.0.0/16",
	"size": 24
    }]
}
```

***FYI!*** A módosításhoz le kell állítani az ***összes*** konténert, eltörölni az összes ***network***-öt és újraindítani a docker daemont!

```
docker kill $(docker ps -q)
docker container prune -f
docker network prune -f
systemctl restart docker.service
```

Ellenőrizzük, hogy milyen network-öt használ a docker!

```
docker network inspect bridge
```
***FIY!*** Nézzük meg, hogy engedélyezve van-e az IPV6 használata:
>"EnableIPv6": false,


## Engedélyezzük az ipv6 használatát a konténerben

Bővítsük a ***daemon.json*** fájlt az alábbi tartalommal:

```
    "ipv6": true,
    "fixed-cidr-v6": "2001:db8:1::/64"
```

Indítsuk újra a docker daemont

```
docker kill $(docker ps -q)
docker container prune -f
docker network prune -f
systemctl restart docker
```

Nézzük meg, hogy ismét, hogy engedélyezve van-e az IPV6 használata:

```
docker network inspect bridge
```
>"EnableIPv6": true,

## Irányítsuk át a konténerek logját a messages-be

Indítsuk el egy nginx konténert

```
docker run -d --name=web -p 80:80 nginx:1.23
```

Nézzük meg a működését egy web browsre-ből miközben megnézzük a log bejegyzéseket.

```
docker logs web

cat /var/log/messages | grep GET
```

Állítsuk le az nginx konténert.

```
docker rm -f web
```

Bővítsük a ***daemon.json*** fájlt az alábbi tartalommal:

```
    "log-driver": "journald",
    "log-opts": {
	"tag": "{{.Name}}"
    }
```

Indítsuk újra a docker és a syslog (journald) daemont

```
systemctl restart docker
systemctl restart systemd-journald
```

Indítsuk újra az nginx konténert

```
docker run -d --name=web -p 80:80 nginx:1.23
```

Nézzük meg a működését egy web browsre-ből miközben megnézzük a log bejegyzéseket.

```
docker logs web

cat /var/log/messages | grep GET
```

## Töltsünk le egy docker image-et ***proxy*** mögül

Tiltsuk le az Internet elérédt biztosító ***enp0s3*** interface-t.

```
nmcli con down enp0s3
```

Próbáljuk letölteni a ***busybox*** docker image-t

```
docker pull busybox
```

Kapcsoljuk be a ***kuma-01** gépet és indítsunk el benne egy squid proxy-t

```
systemctl start squid
tail -f /var/log/squid/access.log
```

A ***kuwo-01***-es gépen engedélyezzük a docker daemonnak a proxy használatot. 
Ehhez hozzuk létre a ***/etc/systemd/system/docker.service.d/*** alá a ***10_docker_proxy.conf*** fájlt az alábbi tartalommal:

```
[Service]
Environment=HTTP_PROXY=http://192.168.56.100:3128
Environment=HTTPS_PROXY=http://192.168.56.100:3128
```
Indítsuk újra a docker service-t

```
systemctl daemon-reload
systemctl restart docker
```

Próbáljuk újra letölteni a ***busybox*** docker image-t

```
docker pull busybox
```

## Állítsuk vissza proxy nélküli működésre a dockert és indítsuk újra a gépet

```
rm -f /etc/systemd/system/docker.service.d/10_docker_proxy.conf
systemctl daemon-reload
systemctl restart docker
nmcli con down enp0s8 && reboot -f
```
