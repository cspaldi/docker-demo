# Dokcer Demó - 18. feladat

> ***Docker hálózat***

## Ellenőrizzük a Docker-es bridge interface (***docker0***) meglétét

```
ip a
```
***FYI!*** Nézzük meg az IP tartományt és a netmask-ot. Ez az első /16-os subnet ami létrejött.

## Kérdezzük meg a dockert milyen hálózatokat ismer

```
docker network ls
```
>Mivel minket csak a bridge típusúak érdekelnek kérdezhetjük így is

```
docker network ls -f driver=bridge
```

## Vizsgáljuk meg mit lehet tudni a ***bridge*** nevű dokker hálózatról

```
docker network inspect bridge
```
***FYI!*** Látszik, hogy ez a docker hálózat a *docker0*-t használja default gateway-nek!

## Kérdezzük le a jelenlegi iptables szabályokat

```
iptables -t nat -L
```
***FYI!***  A futó konténer eléréséhez a docker iptables/nat szabályokat használ. Látszik a hogy a meglevő docker hálózatból a kifelé menő forgalom NAT-olva lesz.

## Indítsunk el egy alpine linux konténert aminek kihúzzuk a 80-as portját a host 80-as portjára

```
docker run -d -it -p 80:80 alpine
```
## Vizsgáljuk meg ismét a ***bridge*** nevű dokker hálózatot

```
docker network inspect bridge
```
***FYI!*** Látszik, hogy a hálózatba létrejött egy konténer ami a 172.17.0.2/16-os IP címet kapta.

## Nézzük meg mi változott az iptables szabályokban

```
iptables -t nat -L
```
***FYI!*** Látszik, hogy a hálózatba létrejött egy új NAT szabály a 172.17.0.2/16-os IP NAT-olja kifelé ha HTTP a forgalom. Ezen kívűl létre jön egy DNAT szabály is, hogy ha a host IP címére érkezik HTTP kérés, azt a 172.17.0.2 es IP 80-as portjára továbbítja!

## Kérdezzük le, hogy a milyen virtuális hálókártya jött létre a futó konténerhez

```
brctl show docker0
```
***FYI!*** Látszik, hogy a host gépen létrejött egy ***vethaxxx*** nevű interface a futó konténernek.

