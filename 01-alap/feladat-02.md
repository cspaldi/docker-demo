# Dokcer Demó - 2. feladat
> ***Interactive terminál***

## Indítsunk el egy alpine linux konténert és ellenőrizzük a futását

```
docker run alpine
docker ps
docker ps -a
```

## Indítsunk el egy alpine linux konténert és futtassunk benne egy parancsot pl. ls

```
docker run alpine ls
```

## Indítsunk el egy alpine linux konténert és lépjünk bele

```
docker run -it alpine
```

Ekkor a konténerben futó linux-ba lépünk be, és itt futtathatunk tetszőleges parancsokat. pl:

```
ls
cat /etc/*release*
hostname
```

Ha egy másik terminálból ellenőrizzük a konténereket, azt látjuk, hogy van egy aktuálisan futó konténerünk.
A kontnerből az **exit** vagy a **ctrl + d** parancsal lehet kilépni, ekkor a konténer is leáll!

