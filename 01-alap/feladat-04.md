# Dokcer Demó - 4. feladat

> ***Interactive terminál, attach, detach***

## Csatlakozzunk a futó konténerünkhöz

```
docker exec -it web sh
```
Csatlakozunk a futó konténerünkhöz annélkül, hogy befolyásoltuk volna az nginx service működését. Csak olyan parancsot tudunk exec-el futtatni (pl, bash, sh) ami telepítve van a konténerbe.<br>
Lépjünk konténerből a **ctrl + d**-vel vagy az **exit** paranccsal, ekkor a konténerünk továbbra is fut.

## Csatlakozzunk a futó konténerünk std. output-jához

```
docker attach web
```
Csatlakozzunk egy web browserrel a host gépünk 8080-as port-jához, és frissítsük párszor az oldalt. Az attach-elt konzolon látjuk az nginx log-ban a browser csatlakozási kisérleteit.

***Mivel "csak" csatlakoztunk (attach) a konténerhez, de a konténert nem interaktív terminállal indítottuk, nem tudunk lecsatlakozni! Kilépni a ctrl + c-vel tudunk, de ezzel a konténerünk is leáll!***

## Töröljük el a web konténert és hozzuk létre újra úgy, hogy legyen interaktív terminálja is 

```
docker rm web
docker run -dit -p 8080:80 --name=web nginx
```
Ekkor ha **attach** paranccsal kapcsolódunk a futó konténerhez, mivel interaktív terminál módban indítottuk, le is lehet csatlakozni (detach) a konténer megállítása nélkül. Az alapértelmezett detach billentyű kombináció **ctrl+p,ctrl+q**

```
docker attach web
```
Amennyiben saját detach billentyű kombinációt akarunk használni, így tehetjük meg:

```
docker attach --detach-keys="a,b,c" web
```

## Kérjük le a futó konténerünk log-ját annélkül, hogy csatlakoznánk (attach)
```
docker logs web
```
Kiírja az eddigi log bejegyzéseket. Amennyiben folyamatosan szeretnénk követni a logokat, a linux-os **tail -f** mintájára itt is használhatjuk a **-f** kapcsolót.
```
docker logs -f web
```
A log-ok kiiratását a **ctrl + c** billenytü kombinációval lehet megszakítani. Ettől a konténerünk nem fog leállni!

## Állítsuk le és töröljük el az nginx konténert

```
docker kill web
docker rm web
```

