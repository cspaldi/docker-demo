# Dokcer Demó - 1. feladat

> ***Hello-world konténer***

## Lépjünk be a /oktatás/docker mappába és itt végezzük el a feladatokat
```
cd /oktatas/docker
```

## Ellenőrizzük a docker service működését a gépen

```
docker info
```

## Indítsuk el az elő Hello-World konténerünket

```
docker run hello-world
```

A hello-world konténer letöltődik a Docker Hub-ról a lokális docker registry-be, és elindul a konténerünk. 

## Indítsuk el mégegyszer a Hello-World konténerünket

```
docker run hello-world
```

Ha újra kiadjuk a parancsot, látszik, hogy már nem töltődik le, hiszen helyileg megvan, csak elindul a konténer.


## Ellenőrizzük, hogy futnak-e a Hello-World konténereink

```
docker ps
```
Nem látszik futó konténer, ezért nézzük meg, hogy futottak-e valaha.

```
docker ps -a
```
A konténereket listázhatjuk tulajdonság alapján is a **-f** vagy **--filter** kapcsoló használatával. <br>  

A lehetséges filter kategóriák:

- id
- name
- status
  - created
  - restarting
  - running
  - removing
  - paused
  - exited
  - dead
  <br>

**pl.** Leállt konténerek listázása
```
docker ps -f status=exited
```

## Töröljük el a Hello-World konténereinket

Egy konténer törléséhez meg kell adnunk a konténer nevét, vagy a konténer ID-t.

```
docker rm ID/Name
```
Ha egyszerre több konténert akarunk törölni, használjuk ezt a módszert

```
docker rm $(docker ps -qa)
```
***Csak azokat a konténereket tudjuk törölni, amik nem futnak!***

