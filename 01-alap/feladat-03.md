# Dokcer Demó - 3. feladat

> ***Port forward***

## Indítsunk el egy nginx konténert és ellenőrizzük a futását

```
docker run nginx
```
Ekkor a konténer elindul, de nem kapjuk vissza a prompt-ot, és a képernyőn a konténer log-ja látszik. Egy másik terminálból ellenőrizzük hogy fut-e.<br><br>
Szakítsuk meg a konténer futását a **ctrl + c**-vel, ekkor a konténerünk is leáll.

## Indítsunk el egy nginx konténert daemon módban és ellenőrizzük a futását

```
docker run -d nginx
docker ps
```

## Ellenőrizzük, hogy valóban kiszolgál-e a 127.0.0.1:80 p 

```
curl http://127.0.0.1
```
A 127.0.0.1-en nem szolgál ki, ezért állítsuk le a konténerünket az ID, vagy a neve megadásával.

```
docker stop ID/Name
```

## Indítsuk el úgy, hogy a konténer 80-as protját kitükrözzük a gépünk 8080-as portjára

```
docker run -d -p 8080:80 nginx
```

Ellenőrizzük egy web browserrel, hogy működik-e az nginx a host gépünk 8080-as port-ján.

## Állítsuk le a konténerünket, ellenőrizzük, hogy eddig milyen konténerek futottak, és töröljük el őket

```
docker stop ID/Name
docker ps -a
docker rm $(docker ps -qa)
```
## Indítsuk el úgy, hogy a konténer 80-as protját kitükrözzük a gépünk 8080-as portjára, és adjunk neki saját nevet

```
docker run -d -p 8080:80 --name web nginx
```
Ellenőrizzük milyen néven jött létre a konténer. 

(***Figyeljünk arra, hogy ha újabb konténert akarunk indítani, azt azonos néven nem lehet!***</strong>***)

```
docker ps
```
## Állítsuk le a konténerünket, majd próbáljuk újra létrehozni a konténerünket

```
docker stop web
docker run -d -p 8080:80 --name web nginx
```
Nem lehet elindítani, mer már van egy ilyen nevű, leállított, de el nem törölt konténer. 

## Indítsuk el újra a web nevű konténerünket

```
docker start web
```
A konténer daemon módban fog elindulni!




